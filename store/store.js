import {applyMiddleware, createStore} from 'redux';
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers";

const inisialState = {};
const middleware = [thunk];

const store = createStore(rootReducer, inisialState, composeWithDevTools(applyMiddleware(...middleware)));

export default store;