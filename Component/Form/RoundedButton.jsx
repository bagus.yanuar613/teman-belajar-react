import React, { Component } from 'react';

class RoundedButton extends Component {
    render() {
        return (
            <a className={`btn ${this.props.class} w-100`} onClick={this.props.onClick}>
                {this.props.title}
            </a>
        );
    }
}

export default RoundedButton;