import React, { Component } from "react";

class TextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
  }

  handleChange = (e) => {
    const input = e.target.value;
    this.setState({
      value: input,
    });
    this.props.onChange(input);
  };
  render() {
    return (
      <div className="form-group">
        <label htmlFor="exampleInputEmail1">Email address</label>
        <input
          value={this.state.value}
          onChange={this.handleChange}
          type="email"
          className="form-control"
          id="exampleInputEmail1"
          aria-describedby="emailHelp"
          placeholder="Enter email"
        />
        <small id="emailHelp" className="form-text text-muted">
          We'll never share your email with anyone else.
        </small>
      </div>
    );
  }
}

export default TextField;
