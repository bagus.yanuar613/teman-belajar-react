import React, { Component } from 'react';

class PromoContent extends Component {
    render() {
        return (
            <div className="container-fluid">
                <p className="title-section">Promo Spesial Buat Kamu</p>
                <div className="promo focus" id="panel-promo">
                    <div className="gen-card-promo shine-rounded-md w-100 mr-1 ml-1" style={{ height: '7em' }}></div>
                </div>
            </div>
        );
    }
}

export default PromoContent;