import React, { Component } from 'react';

class OtherCategories extends Component {
    render() {
        return (
            <section className="container-fluid">
                <p className="title-section">Semua Ada Gurunya</p>
                <div className="row" id="panel-category">
                    <div className="col col-3">
                        <a href="/pencarian-kategori" className="bt-mini">
                            <div className="imagebutton shine-rounded-md "></div>
                        </a>
                    </div>
                </div>
            </section>
        );
    }
}

export default OtherCategories;