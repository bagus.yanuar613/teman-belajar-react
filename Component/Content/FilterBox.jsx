import axios from "axios";
import React, { Component } from "react";
import LinkWithModal from "../Element/LinkWithModal";
import MethodOption from "../Element/MethodOption";
import RoundedButton from "../Form/RoundedButton";
import ModalCategory from "../Modal/ModalCategory";
import ModalDistrict from "../Modal/ModalDistrict";


class FilterBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      category: null,
      categoryName: '',
      method: 1,
      districtName: 'Pilih Kecamatan',
      district: null,
      categoryShimmer: true,
      categoryModal: false,
      districtModal: false
    }
  }

  getReferenceCategory = () => {
    axios.get("http://localhost:8002/api-public/v2/categories/reference").then((res) => {
      let data = res.data.payload.data;
      if (data !== null) {
        this.setState({
          category: data.id,
          categoryName: data.name,
          categoryShimmer: false
        });
      }
    }).catch((err) => {
      console.log(err);
    });
  }

  handleChangeMethod = (v) => {
    this.setState({
      method: v
    })
  }

  handleOpenCategoryModal = () => {
    this.setState({
      categoryModal: true
    })
  }
  handleClosCategoryModal = (e, id, name) => {
    if (id !== undefined || name !== undefined) {
      this.setState({
        category: id,
        categoryName: name,
      })
    }
    this.setState({
      categoryModal: false
    })
  }

  handleOpenDistrictModal = () => {
    this.setState({
      districtModal: true
    })
  }
  handleCloseDistrictModal = (e, id, name) => {
    console.log(id, name);
    if (id !== undefined || name !== undefined) {
      this.setState({
        district: id,
        districtName: name,
      })
    }
    this.setState({
      districtModal: false
    })
  }

  findTentor = () => {
    if (this.state.method === 1) {
      if (this.state.district === null) {
        alert('Wilayah')
      } else {
        window.location.href = '/pencarian?category=' + this.state.category + '&district=' + this.state.district + '&method=' + this.state.method
      }
    } else {
      window.location.href = '/pencarian?category=' + this.state.category + '&method=' + 2
    }
  }
  componentDidMount() {
    this.getReferenceCategory();
  }

  render() {
    return (
      <section className="mt-2 container-fluid">
        <h1 className="title-section ">Temukan Guru Les Privat Terbaikmu</h1>
        <LinkWithModal openModal={this.handleOpenCategoryModal} shimmer={this.state.categoryShimmer} label="Mata Pelajaran" value={this.state.categoryName} faClass="fa fa-book" />
        <MethodOption onChange={this.handleChangeMethod} />
        { this.state.method === 1 && <LinkWithModal openModal={this.handleOpenDistrictModal} label="Lokasi Belajar" value={this.state.districtName} faClass="fa fa-map-marker" />}
        <div className="gen-pilihan container-fluid">
          <RoundedButton class="bt-accent mt-1 text-white" title="Cari Guru Les Privat" onClick={this.findTentor} />
        </div>
        <ModalCategory show={this.state.categoryModal} closeModal={this.handleClosCategoryModal} />
        <ModalDistrict show={this.state.districtModal} closeModal={this.handleCloseDistrictModal} />
      </section>
    );
  }
}

export default FilterBox;
