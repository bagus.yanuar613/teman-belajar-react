import React, { Component } from "react";
import Spinner from "react-bootstrap/Spinner";

class ModalLoading extends Component {
  render() {
    return (
      <div
        className="w-100 text-center d-flex align-items-center justify-content-center"
        style={{ minHeight: 200 }}
      >
        <div className="text-center">
          <Spinner animation="grow" variant="secondary" role="status" size="sm"/>
          <Spinner animation="grow" variant="secondary" role="status" size="sm"/>
          <Spinner animation="grow" variant="secondary" role="status" size="sm"/>
          <div className="text-center w-100">{this.props.text}</div>
        </div>
      </div>
    );
  }
}

export default ModalLoading;
