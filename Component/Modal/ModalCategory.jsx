import React, { Component } from 'react';
import Modal from "react-bootstrap/Modal";
import ModalBody from "react-bootstrap/ModalBody";
import axios from "axios";

class ModalCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            popularCategories: [],
        }
    }

    componentDidMount() {
        axios
            .get("http://localhost:8002/api-public/v2/categories/popular")
            .then((res) => {
                this.setState({
                    isLoading: false,
                    popularCategories: res.data.payload.data,
                });
            });
    }
    render() {
        return (
            <Modal show={this.props.show} backdrop="static" keyboard={false}>
                <ModalBody className="p-3" style={{ minHeight: "100vh" }}>
                    <div
                        className="text-left  mb-3 d-flex justify-content-md-center align-content-center border-bottom w-100"
                        style={{ paddingBottom: ".1em" }}
                    >
                        <p className="flex-grow-1" style={{ flexGrow: 1 }}>
                            Cari Pelajaran Apa?
                        </p>
                        <a className="text-danger" onClick={this.props.closeModal}>
                            <i className="fa fa-close"></i>
                        </a>
                    </div>
                    <div style={{ minHeight: "100vh" }}>
                        <div className="d-block">
                            <div className="d-flex align-items-stretch">
                                <input
                                    type="text"
                                    className="form-control flex-grow-1 mr-2"
                                    id="find-category-field"
                                    placeholder="Belajar Apa ?"
                                />
                                <div className=" d-flex justify-content-center  ">
                                    <a
                                        id="btn-find-category"
                                        className="bg-primary  d-flex justify-content-center align-items-center "
                                        style={{ borderRadius: 50, padding: "0 1em" }}
                                    >
                                        <i className="fa fa-arrow-right text-white"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <section className="r-bottom" style={{ paddingBottom: 10 }}>
                            <h2 className="text-left pt-3 f12 pl-2 pr-2 font-weight-bold">
                                Pencarian Populer</h2>
                        </section>
                        <section className="container-fluid" id="category-content">
                            <Chip data={this.state.popularCategories} onClick={this.props.closeModal} />
                        </section>
                        <div className="divider-test"></div>
                        <section
                            style={{ minHeight: "100vh" }}
                            className="container-fluid"
                            id="category-results"
                        ></section>
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}

export default ModalCategory;

class Chip extends Component {
    constructor(props) {
        super(props);
    }

    handleClick = (e) => {
        e.preventDefault();
        let id = e.currentTarget.dataset.id;
        let name = e.currentTarget.dataset.name;
        this.props.onClick(e, id, name)
    };
    render() {
        if (this.props.data.length <= 0) {
            return <div>Data Kosong</div>;
        }
        return (
            <div className="d-flex flex-wrap">
                {this.props.data.map((v, k) => {
                    return (
                        <a
                            key={k}
                            className="chip-primary mr-1 mb-1 chip-category-target"
                            data-id={v.id}
                            data-name={v.name}
                            href="#"
                            onClick={this.handleClick}
                        >
                            {v.name}
                        </a>
                    );
                })}
            </div>
        );
    }
}