import React, { Component } from 'react';
import Modal from "react-bootstrap/Modal";
import ModalBody from "react-bootstrap/ModalBody";
import ModalLoading from '../Loader/ModalLoading';
import axios from "axios";

class ModalDistrict extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            inputValue: '',
            onDataLoading: false
        }
    }

    handleChange = (e) => {
        this.setState({
            inputValue: e.currentTarget.value
        })
    }
    getDistrictByName = () => {
        let name = this.state.inputValue;
        if (name !== '') {
            this.setState({
                onDataLoading: true
            })
            axios
                .get("http://localhost:8002/api-public/v2/district?name=" + name)
                .then((res) => {
                    console.log(res.data);
                    this.setState({
                        onDataLoading: false,
                        data: res.data.payload.data,
                    });
                });
        }
    }
    render() {
        return (
            <Modal show={this.props.show} backdrop="static" keyboard={false}>
                <ModalBody className="p-3" style={{ minHeight: "100vh" }}>
                    <div
                        className="text-left  mb-3 d-flex justify-content-md-center align-content-center border-bottom w-100"
                        style={{ paddingBottom: ".1em" }}
                    >
                        <p className="flex-grow-1" style={{ flexGrow: 1 }}>
                            Mau Belajar Di Mana?
                        </p>
                        <a className="text-danger" onClick={this.props.closeModal}>
                            <i className="fa fa-close"></i>
                        </a>
                    </div>
                    <div style={{ minHeight: "100vh" }}>
                        <div className="d-block">
                            <div className="d-flex align-items-stretch">
                                <input
                                    type="text"
                                    className="form-control flex-grow-1 mr-2"
                                    id="district-field"
                                    placeholder="Mau Belajar Di Mana?"
                                    value={this.state.inputValue}
                                    onChange={this.handleChange}
                                />
                                <div className=" d-flex justify-content-center  ">
                                    <a
                                        id="btn-find-district"
                                        className="bg-primary  d-flex justify-content-center align-items-center "
                                        style={{ borderRadius: 50, padding: "0 1em" }}
                                        onClick={this.getDistrictByName}
                                    >
                                        <i className="fa fa-arrow-right text-white"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="divider-test"></div>
                        <section
                            style={{ minHeight: "100vh" }}
                            className="container-fluid"
                            id="district-results"
                        >
                            {this.state.onDataLoading === true ? <ModalLoading text="Sedang Memuat Data..." /> : <List data={this.state.data} onClick={this.props.closeModal}/>}
                        </section>
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}

export default ModalDistrict;
class List extends Component {
    constructor(props) {
        super(props);
    }

    handleClick = (e) => {
        e.preventDefault();
        let id = e.currentTarget.dataset.id;
        let name = e.currentTarget.dataset.name;
        this.props.onClick(e, id, name)
    };

    render() {
        if (this.props.data.length <= 0) {
            return <div>Data Kosong</div>;
        }
        return (
            <div>
                {this.props.data.map((v, k) => {
                    const { id, districtName } = v;
                    return <div key={k} className="hasil-pencarian">
                        <a href="#"
                            data-id={id}
                            data-name={districtName}
                            onClick={this.handleClick}
                        >
                            {`${districtName} (${v.subCity.name})`}
                        </a>
                    </div>
                })}
            </div>
        );
    }
}