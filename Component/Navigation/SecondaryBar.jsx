import React, { Component } from 'react';

class SecondaryBar extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-white sticky-top container-fluid shadow-sm flex-wrap">
                <div className="d-flex justify-content-center">
                    <div className="rounded-lg2 d-flex justify-content-md-center align-content-center" style={{ height: 50, width: 50 }}>
                        <a href="" className="align-self-center text-dark">
                            <i className="mdi mdi-chevron-left centertodiv f16"></i>
                        </a>
                    </div>
                </div>
                <div className="p-2 flex-grow-1 d-flex justify-content-md-center">
                    <a id="title-bar" className="align-self-start text-white font-weight-bold text-dark">{this.props.title}</a>
                </div>
                {this.props.children}
            </nav>
        );
    }
}

export default SecondaryBar;