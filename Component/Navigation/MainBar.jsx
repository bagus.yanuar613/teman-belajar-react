import React, { Component } from "react";

class MainBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawer: false,
    };
  }

  togleSidebar = () => {
    this.setState({
      drawer: !this.state.drawer,
    });
    console.log(this.state.drawer);
  };

  openSidebar = () => {
    this.setState({
      drawer: true,
    });
  };
  closeSidebar = () => {
    this.setState({
      drawer: false,
    });
  };
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-primary sticky-top">
          <div className="d-flex justify-content-md-center w-100">
            <div
              className="rounded-lg2 d-flex justify-content-md-center align-content-center"
              style={{ height: 50, width: 50 }}
            >
              <a
                className="align-self-center openDrawer"
                onClick={this.openSidebar}
              >
                <i className="fa fa-bars fa-lg centertodiv text-white"></i>
              </a>
            </div>
            <div
            className="p-2 w-100 d-flex justify-content-md-center align-content-center"
          >
            <img
              src="images/common/logo.png"
              className="centertodiv"
              height="40px"
              alt="gambar-logo"
            />
          </div>
          </div>
          
        </nav>
        <div
          className={`backdrop-drawer ${this.state.drawer ? "open" : "close"}`}
          onClick={this.closeSidebar}
        ></div>
        <div
          className={`drawer ${this.state.drawer ? "open" : "close"}`}
          onClick={this.closeSidebar}
        ></div>
      </div>
    );
  }
}

export default MainBar;
