import React, { Component } from "react";

import ModalLoading from "../Loader/ModalLoading";

class CategoryModalContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      popularCategories: [],
    };
  }

  handleChipClick = (id, name) => {
      console.log(id, name);
      this.props.closeModal()
  }
  

  render() {
    if (this.state.isLoading) {
      return <ModalLoading text="Sedang Memuat Pelajaran Terpopuler..." />;
    }
    return (
      <div style={{ minHeight: "100vh" }}>
        <div className="d-block">
          <div className="d-flex align-items-stretch">
            <input
              type="text"
              className="form-control flex-grow-1 mr-2"
              id="find-category-field"
              placeholder="Belajar Apa ?"
            />
            <div className=" d-flex justify-content-center  ">
              <a
                id="btn-find-category"
                className="bg-primary  d-flex justify-content-center align-items-center "
                style={{ borderRadius: 50, padding: "0 1em" }}
              >
                <i className="fa fa-arrow-right text-white"></i>
              </a>
            </div>
          </div>
        </div>
        <section className="r-bottom" style={{ paddingBottom: 10 }}>
          <h2 className="text-left pt-3 f12 pl-2 pr-2 font-weight-bold">
            Pencarian Populer
          </h2>
        </section>
        <section className="container-fluid" id="category-content">
          <Chip data={this.state.popularCategories} onClick={this.handleChipClick}/>
        </section>
        <div className="divider-test"></div>
        <section
          style={{ minHeight: "100vh" }}
          className="container-fluid"
          id="category-results"
        ></section>
      </div>
    );
  }
}

export default CategoryModalContent;

class Chip extends Component {
  constructor(props) {
    super(props);
  }

  handleClick = (e) => {
    e.preventDefault();
    let id = e.currentTarget.dataset.id;
    let name = e.currentTarget.dataset.name;
    this.props.onClick(id, name)
  };
  render() {
    if (this.props.data.length <= 0) {
      return <div>Data Kosong</div>;
    }
    return (
      <div className="d-flex flex-wrap">
        {this.props.data.map((v, k) => {
          console.log(v);
          return (
            <a
              key={k}
              className="chip-primary mr-1 mb-1 chip-category-target"
              data-id={v.id}
              data-name={v.name}
              href="#"
              onClick={this.handleClick}
            >
              {v.name}
            </a>
          );
        })}
      </div>
    );
  }
}
