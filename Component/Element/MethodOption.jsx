import React, { Component } from 'react';

class MethodOption extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 1
        }
    }
    
    handleClick = (e) => {
        this.setState({
            value: parseInt(e.currentTarget.value)
        }, this.props.onChange(parseInt(e.currentTarget.value)))
    }

    render() {

        return (
            <div className="gen-pilihan container-fluid">
                <a className="label">Metode Belajar</a>
                <div className="content">
                    <div className="d-flex w-100 mt-3 mb-3">
                        <input className="checkbox-jam w-100 method" type="radio" name="jenis" id="offline"
                            value="1" defaultChecked={ this.state.value === 1 ? true : false } onChange={this.handleClick}/>
                        <label className="for-checkbox-jam w-100 flex-grow-1" htmlFor="offline">
                            Tatap Muka
                    </label>
                        <input className="checkbox-jam w-100 method" type="radio" name="jenis" value="2"
                            id="online" defaultChecked={ this.state.value === 2 ? true : false } onChange={this.handleClick}/>
                        <label className="for-checkbox-jam w-100 flex-grow-1" htmlFor="online">
                            Online
                    </label>
                    </div>
                    <hr />
                </div>
            </div>
        );
    }
}

export default MethodOption;