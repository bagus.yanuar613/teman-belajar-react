import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import ModalBody from "react-bootstrap/ModalBody";

class LinkWithModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };
  }

  openModal = () => {
    this.setState({
      modal: true,
    });
  };

  closeModal = () => {
    this.setState({
      modal: false,
    });
  };
  render() {
    return (
      <div>
        <div className="gen-pilihan container-fluid">
          <a className="label">{this.props.label}</a>
          <div className="content">
            <i className={`icon ${this.props.faClass}`}></i>
            {
              this.props.shimmer ? <a className="gen-pilihan container-fluid shine-rounded-sm" style={{height: '2em'}}></a> :
                <a
                  id="category"
                  data-category=""
                  className="isi"
                  onClick={this.props.openModal}
                >
                  {this.props.value}
                </a>
            }

          </div>
          <hr />
        </div>
      </div>
    );
  }
}

export default LinkWithModal;
