import axios from 'axios';
import React, { Component } from 'react';
import ModalLoading from '../Loader/ModalLoading';

function List(props) {
    if (props.data.length <= 0) {
        return <div>Data Kosong</div>;
    }
    console.log(props);
    return (
        <div>
            {props.data.map((v, k) => {
                const { id, districtName } = v;
                return <div key={k} className="hasil-pencarian">
                    <a data-id={id} data-name={districtName}>{`${districtName} (${v.subCity.name})`}</a>
                </div>
            })}
        </div>
    );
}
class DistrictModalContent extends Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
            inputValue: '',
            onDataLoading: false
        }
    }

    handleChange = (e) => {
        this.setState({
            inputValue: e.currentTarget.value
        })
    }
    getDistrictByName = () => {
        let name = this.state.inputValue;
        if (name !== '') {
            this.setState({
                onDataLoading: true
            })
            axios
                .get("http://localhost:8002/api-public/v2/district?name=" + name)
                .then((res) => {
                    console.log(res.data);
                    this.setState({
                        onDataLoading: false,
                        data: res.data.payload.data,
                    });
                });
        }
    }

    componentDidMount() {
        console.log('call modal');

    }

    render() {
        return (
            <div style={{ minHeight: "100vh" }}>
                <div className="d-block">
                    <div className="d-flex align-items-stretch">
                        <input
                            type="text"
                            className="form-control flex-grow-1 mr-2"
                            id="find-category-field"
                            placeholder="Mau Belajar Di Mana?"
                            value={this.state.inputValue}
                            onChange={this.handleChange}
                        />
                        <div className=" d-flex justify-content-center  ">
                            <a
                                id="btn-find-category"
                                className="bg-primary  d-flex justify-content-center align-items-center "
                                style={{ borderRadius: 50, padding: "0 1em" }}
                                onClick={this.getDistrictByName}
                            >
                                <i className="fa fa-arrow-right text-white"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div className="divider-test"></div>
                <section
                    style={{ minHeight: "100vh" }}
                    className="container-fluid"
                    id="district-panel"
                >
                    {this.state.onDataLoading === true ? <ModalLoading text="Sedang Memuat Data..." /> : <List data={this.state.data} />}
                </section>
            </div>
        );
    }
}

export default DistrictModalContent;