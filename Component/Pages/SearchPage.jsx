import React, { Component } from "react";
import SecondaryBar from "../Navigation/SecondaryBar";
import Head from "next/head";
import axios from "axios";
import { BaseUrl } from "../Config/Url";

class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      method: this.props.method,
      category: this.props.dataCategory,
      district: this.props.district,
      districtName: "",
      level: [],
      isLevelReady: false
    };
  }

  componentDidMount() {
    axios
      .get(BaseUrl + "/api-public/v2/district/by/" + this.state.district)
      .then((res) => {
        let data = res.data.payload.data;
        this.setState({
          districtName: data.districtName + ", " + data.subCity.name,
        });
      });
    axios
      .get(
        BaseUrl + "/api-public/v2/level/category/by/" + this.props.category.id
      )
      .then((res) => {
        let data = res.data.payload.data;
        this.setState({
          level: data,
          isLevelReady: true
        });
      });
  }

  handleClick = (id) => {
    console.log(id);
  };
  render() {
    return (
      <div>
        <Head>
          <title>Teman Belajar | Pencarian Guru</title>
          <link rel="icon" href="/favicon.ico" />
          <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossOrigin="anonymous"
          ></link>
          <link
            href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap"
            rel="stylesheet"
          ></link>
          <link
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            rel="stylesheet"
          ></link>
        </Head>
        <SecondaryBar title={this.props.category.name}>
          <div
            className="mt-1 text-center mb-1"
            style={{
              width: "100%",
              padding: "0.5em",
              backgroundColor: "var(--primaryColorLight)",
              border: "1px solid var(--primaryColor)",
              borderRadius: 5,
              color: "var(--primaryColor)",
            }}
          >
            {this.state.districtName}
          </div>
          {this.state.isLevelReady ? <ChipLevel data={this.state.level} onClick={this.handleClick} /> : ''}
        </SecondaryBar>
      </div>
    );
  }
}

export default SearchPage;

class ChipLevel extends Component {
  constructor(props) {
    super(props);
  }

  handleClick = (e) => {
    let id = e.currentTarget.value;
    this.props.onClick(id);
  };

  componentDidMount() {
      console.log('chip ready');
      
  }
  
  render() {
    return (
      <div className="d-flex mt-3 w-100">
        {this.props.data.map((v, k) => {
          return (
            <div key={k}>
              <input
                onChange={this.handleClick}
                className="checkbox-jam level-choice"
                type="radio"
                name="level"
                value={v.level.id}
                id={`level-${v.level.id}`}
                defaultChecked={k===0 ? 'checked' : ''}
              />
              <label
                className="for-checkbox-jam"
                htmlFor={`level-${v.level.id}`}
                style={{
                  padding: ".4em .8em",
                }}
              >
                {v.level.name}
              </label>
            </div>
          );
        })}
      </div>
    );
  }
}
