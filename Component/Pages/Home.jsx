import React, { Component } from "react";
import Head from "next/head";
import MainBar from "../Navigation/MainBar";
import FilterBox from "../Content/FilterBox";
import PromoContent from "../Content/PromoContent";
import OtherCategories from "../Content/OtherCategories";

class Home extends Component {
  render() {
    return (
      <div>
        <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
          <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossOrigin="anonymous"
          ></link>
          <link
            href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap"
            rel="stylesheet"
          ></link>
          <link
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            rel="stylesheet"
          ></link>
        </Head>
        <MainBar />
        <section className="bg-primary round-bottom">
          <h2 className="text-white text-center pt-3 f20">Hybird Learning</h2>
          <p className="text-white text-center pt-4 pr-2 pl-2 f10">
            Belajar jadi lebih mudah dengan metode hybrid learning yang
            mengintegrasikan pembelajaran online dan offline (tatap muka).
          </p>
          <p className="text-white text-center pt-1 pr-2 pl- pb-5 f10">
            Belajar, Berlatih, Berprestasi <b>#PintarDenganTemanBelajar</b>
          </p>
        </section>
        <FilterBox />
        <PromoContent />
        <OtherCategories />
      </div>
    );
  }
}

export default Home;
