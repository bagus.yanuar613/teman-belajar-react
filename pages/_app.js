import '../styles/globals.css'
import '@mdi/font/css/materialdesignicons.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
