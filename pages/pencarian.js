import SearchPage from "../Component/Pages/SearchPage";
import { BaseUrl } from "../Component/Config/Url";

function SearchPages({ query, dataCategory, district, method }) {
  return <SearchPage query={query} category={dataCategory.payload.data} district={district} method={method} />;
}

export async function getServerSideProps(context) {
  let query = context.query;
  let availableMethod = [1, 2];
  let method = parseInt(query.method);
  let category = parseInt(query.category);
  let district = null;
  if (!availableMethod.includes(method)) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  if (method === 1) {
    district = parseInt(query.district);
  }
  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }
  let res = await fetch(BaseUrl + "/api-public/v2/categories/by/" + category, options);
  let dataCategory = await res.json();
  if (!dataCategory) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      query, dataCategory, district, method
    },
  };
}
export default SearchPages;
